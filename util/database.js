import { Sequelize } from 'sequelize';
import { dbConfig } from '../config/appConfig';
const { username, password, database } = dbConfig.credentials;

const sequelize = new Sequelize(database, username, password, dbConfig.env);

export default sequelize;