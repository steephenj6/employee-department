import httpStatus, { NOT_FOUND } from 'http-status';
import { ValidationError } from 'express-validation';
import APIError from '../error/apiError';

/**
 * Error handler.
 * @public
 */
const handler = (err, req, res, next) => {
  const response = {
    code: err.status,
    message: err.message || httpStatus[err.status],
    errors: err.errors,
    stack: err.stack,
  };

  res.status(err.status);
  res.json(response);
  res.end();
};

/**
 * If error is not an instanceOf APIError, convert it.
 * @public
 */
const convertError = (err, req, res, next) => {
  let convertedError = err;

  if (err instanceof ValidationError) {
    convertedError = new APIError({
      message: 'Validation Error',
      errors: err.errors,
      status: err.status,
      stack: err.stack,
    });
  } else if (!(err instanceof APIError)) {
    convertedError = new APIError({
      message: err.message,
      status: err.status,
      stack: err.stack,
    });
  }

  return handler(convertedError, req, res);
};

/**
 * Catch 404 and forward to error handler
 * @public
 */
const notFound = (req, res, next) => {
  const err = new APIError({
    message: 'Not found',
    status: NOT_FOUND,
  });
  return handler(err, req, res);
};

export default {
  convertError,
  notFound
};
