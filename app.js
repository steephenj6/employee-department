import './config/appConfig';

import express from 'express';
import { json, urlencoded } from 'body-parser';
import { sync } from './util/database';
import { notFound, convertError } from './middleware/errorMiddleware';

import Employee from './models/employees';
import Department from './models/departments';
import { belongsTo } from './models/employeeDepartment';

import empRoutes from './routes/employees';
import depRoutes from './routes/departments';


/**
* Express instance
* @public
*/
const app = express();

// parse body params and attaches them to req.body
app.use(json());
app.use(urlencoded({ extended: true }));

// API routes
app.use('/employees', empRoutes);
app.use('/departments', depRoutes);

// Error Middlewares
app.use(notFound);
app.use(convertError);

// Employee.hasMany(EmpDept);
belongsTo(Employee, {
    foreignKey: {
        name: 'empId'
    },
    onDelete: 'CASCADE'
});

// Department.hasMany(EmpDept);
belongsTo(Department, {
    foreignKey: {
        name: 'deptId'
    },
    onDelete: 'CASCADE'
});


sync()
    .then(result => {
        console.log('Listening for requests at http://localhost:7001');
        app.listen(7001);
    })
    .catch(err => {
        console.log(err);
    });