import { DataTypes, UUIDV4 } from 'sequelize';
import { define } from '../util/database';

const Department = define('department', {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: UUIDV4
    },
    name: {
        type: Sequelize.STRING,
        allowNull:false
    }
});

export default Department;