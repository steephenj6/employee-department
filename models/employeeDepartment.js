import { DataTypes, UUIDV4 } from 'sequelize';
import { define } from '../util/database';

const EmpDept = define('employee_department', {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: UUIDV4
    },
    empId: {
        type: DataTypes.UUIDV4,
        allowNull: false
    },
    deptId: {
        type: DataTypes.UUIDV4,
        allowNull: false
    }
});

export default EmpDept;