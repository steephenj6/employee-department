import { DataTypes, UUIDV4 } from 'sequelize';
import { define } from '../util/database';

const Employee = define('employee', {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: UUIDV4
    },
    name: {
        type: DataTypes.STRING,
        allowNull:false
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    isActive: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
    }
});

export default Employee;