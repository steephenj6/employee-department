import { Router } from 'express';
import validate from 'express-validation';
import { getAllDepartments, getDepartment, postDepartment, editDepartment, deleteDepartment } from '../controller/departments';
import { postDepartmentBodySchema, editDepartmentBodySchema } from '../validation/joiRequestValidation';

const router = Router();

// GET => /departments
router.get('/', getAllDepartments);

// GET => /departments/id
router.get('/:id', getDepartment);

// POST => /departments
router.post('/', validate(postDepartmentBodySchema), postDepartment);

// PUT => /departments/id
router.put('/:id', validate(editDepartmentBodySchema), editDepartment);

// DELETE => /departments/id
router.delete('/:id', deleteDepartment);

export default router;