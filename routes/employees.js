import { Router } from 'express';
import validate from 'express-validation';
import { getAllEmployees, getEmployee, postEmployee, editEmployee, deleteEmployee, getEmployeeDepartments, postEmployeeDepartment } from '../controller/employees';
import { idParamsSchema, postEmployeeBodySchema, editEmployeeBodySchema, postEmployeeDepartmentBodySchema } from '../validation/joiRequestValidation';

const router = Router();

// GET => /employees
router.get('/', getAllEmployees);

// GET => /employees/id
router.get('/:id', validate(idParamsSchema), getEmployee);

// POST => /employees
router.post('/', validate(postEmployeeBodySchema), postEmployee);

// PUT => /employees/id
router.put('/:id', validate(editEmployeeBodySchema), editEmployee);

// DELETE => /employees/id
router.delete('/:id', deleteEmployee);

// GET => /employees/id/departments
router.get('/:id/departments', getEmployeeDepartments);

// POST => /employees/id/departments
router.post('/:id/departments', validate(postEmployeeDepartmentBodySchema), postEmployeeDepartment);

export default router;