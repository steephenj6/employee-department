import { INTERNAL_SERVER_ERROR } from 'http-status';

class APIError extends Error {
    constructor({
      message,
      errors,
      stack,
      status = INTERNAL_SERVER_ERROR,
      isPublic = false,
    }) {
      super(message);
      this.name = this.constructor.name;
      this.message = message;
      this.errors = errors;
      this.status = status;
      this.isPublic = isPublic;
      this.stack = stack;
    }
  }
  
  export default APIError;
  